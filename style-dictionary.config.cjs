const StyleDictionary = require("style-dictionary").extend({
  source: ["tokens/**/*.json"],
  platforms: {
    css: {
      transformGroup: "css",
      buildPath: "dist/css/",
      files: [
        {
          format: "css/variables",
          destination: "variables.css",
        },
      ],
    },
  },
});

StyleDictionary.buildAllPlatforms();
